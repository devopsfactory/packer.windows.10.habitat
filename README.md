# packer.windows.10.habitat #

Packer-based Windows 10 evaluation image in regards to Chef's Habitat for Windows.

### What is this repository for? ###

* Evaluation
* Testing
* Learning

### Feature List ###

* Windows 10 Enterprise Evaluation (90 days)
* Powershell 5
* WinRm Enabled and Listening
* OpenSSH Enabled and Listening
* RDP Enabled and Listening
* Chef Client
* Rust Lang
* 7 Zip
* All Windows Updates
* Defrag
* Disk Zeroed
* 60 GB Disk, 45 GB Free
* Vagrant box size 5.5GB

### What's missing (future) ###

* Sysprep, rearm, unattend.xml (2nd one)
* Set habitat background
* Setup automated builds and publish binaries to S3
* Provide Kitchen Test example


### How do I get set up? ###

* Takes about 2 hours to build
* Drop packer binary into folder after clone and run: 
```
./packer build windows_10_hab.json

```


### Contribution guidelines ###

* Fork and pull request


### Who do I talk to? ###

* Contact us on Slack [https://habitat-sh.slack.com/messages/windows/](Link URL)
* Post an Issue here [https://bitbucket.org/devopsfactory/packer.windows.10.habitat/issues?status=new&status=open](Link URL)
* Maintained by Peter Styk


![HabitatWindows10.png](https://bitbucket.org/repo/M8eGj9/images/3538832728-HabitatWindows10.png)

![Screen Shot 2016-07-19 at 11.56.27.png](https://bitbucket.org/repo/M8eGj9/images/2884753443-Screen%20Shot%202016-07-19%20at%2011.56.27.png)

![Screen Shot 2016-07-19 at 11.44.06.png](https://bitbucket.org/repo/M8eGj9/images/1344162421-Screen%20Shot%202016-07-19%20at%2011.44.06.png)

### Credits ###

Countless hours saved thanks to:  

* Joe Fitzgerald https://github.com/joefitzgerald/packer-windows  
* Matt Wrock https://github.com/mwrock/packer-templates