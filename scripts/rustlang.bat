if not exist "C:\Windows\Temp\rust-1.10.0-x86_64-pc-windows-gnu.msi" (
  powershell -Command "(New-Object System.Net.WebClient).DownloadFile('https://static.rust-lang.org/dist/rust-1.10.0-x86_64-pc-windows-gnu.msi', 'C:\Windows\Temp\rust-1.10.0-x86_64-pc-windows-gnu.msi')" <NUL
)

msiexec /qb /i C:\Windows\Temp\rust-1.10.0-x86_64-pc-windows-gnu.msi
powershell -Command "Start-Sleep 1" <NUL

setx PATH "%PATH%;C:\Program Files\Rust stable GNU 1.10\bin"

