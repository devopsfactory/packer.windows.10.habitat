$local="C:\Windows\Temp\7z920-x64.msi"
$remote="http://www.7-zip.org/a/7z920-x64.msi"
if (-NOT (Test-Path $local)) {(New-Object System.Net.WebClient).DownloadFile($remote, $local)}
& msiexec /qb /i $local

$local="C:\Windows\Temp\BGInfo.zip"
$remote="https://download.sysinternals.com/files/BGInfo.zip"
if (-NOT (Test-Path $local)) {(New-Object System.Net.WebClient).DownloadFile($remote, $local)}
& "C:\Program Files\7-Zip\7z.exe" x $local -oC:\BGInfo -aoa

$local="C:\BGInfo\HabitatBackgroundHDv01.jpg"
$remote="https://s3-eu-west-1.amazonaws.com/devopsfactory/habitat/HabitatBackgroundHDv01.jpg"
if (-NOT (Test-Path $local)) {(New-Object System.Net.WebClient).DownloadFile($remote, $local)}

$local="C:\BGInfo\bginfoconfig.bgi"
$remote="https://s3-eu-west-1.amazonaws.com/devopsfactory/habitat/bginfoconfig.bgi"
if (-NOT (Test-Path $local)) {(New-Object System.Net.WebClient).DownloadFile($remote, $local)}

$local="C:\BGInfo\bginfolaunch.bat"
$remote="https://s3-eu-west-1.amazonaws.com/devopsfactory/habitat/bginfolaunch.bat"
if (-NOT (Test-Path $local)) {(New-Object System.Net.WebClient).DownloadFile($remote, $local)}

$registryPath = "HKLM:Software\Microsoft\Windows\CurrentVersion\Run"
$name = "BGInfo"
$value = "C:\BGInfo\bginfolaunch.bat"
IF(!(Test-Path $registryPath)){
    New-Item -Path $registryPath -Force | Out-Null
    New-ItemProperty -Path $registryPath -Name $name -Value $value -PropertyType String -Force | Out-Null
} ELSE {
    New-ItemProperty -Path $registryPath -Name $name -Value $value -PropertyType String -Force | Out-Null
}
& "C:\BGInfo\bginfolaunch.bat" 
